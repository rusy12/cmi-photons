 function ComptonC_rekonstrukce(nCoincidence,MCMCsweeps,density_radius,fig_path,varargin)
   %	zpetna rekonstrukce smeru letu fotonu pomoci Comptonovy kamery.
   %	vyuziva PTRAC data zpracovana skriptem "UTEF_ptrac_2019.m"
   %		nCoincidence: ... kolik koincidenci bude pouzito pro rekonstrukci
   %		MCMCsweeps:              ... kolik kroku bude mit Markov Chain Monte Carlo (pocet kroku = MCMCsweeps*nCoincidence)
   %		varargin: = 'nejistoty', 0/1/2/3, 0/1 ... prvni cislo udava metodu simulace nejistoty energie: 0: zadna nejistota, 
   %		                                          1: rozmazat pouze mensi energii, vetsi dopocitat z celkove energie, 
   %		                                          2: rozmazat obe energie, spocitat nejpravdepodobnejsi energii (stredni hodnotu), 
   %		                                          3: stejne jako (2), ale nejpravdepodobnejsi energie je samplovana z normalniho rozdeleni se stredem  v energii ziskane v (2)
   %                                               druhe cislo udava metodu simulace nejistoty polohy: 0: zadna nejistota, 1: rozmazat polohu uvnitr daneho pixelu
   %		varargin: = 'xsec',0/1 ...  urcuje, zda se bude akceptovani novych MCMC kroku ridit i velikosti ucinneho prurezu; uplatnuje se pouze v pripade 'nejistoty', 3, ...
   %        varargin: = 'hweight', cislo okolo 1.0 ... nastavuje preferenci blizke (>1.0) ci vzdalene (<1.0) vzdalenosti pocatku (=zdroje fotonu) od bodu interakce s detektorem - toho se da vyuzit ke kompenzaci energetickeho rozmazani, ktere umele posouva  pocatek blize k detektoru
   %
   %	=== OSY ====
   %	X: normala k rovine senzoru; svisla osa s kladnym smerem nahoru (tzn.smerem dovnitr detektoru je to -Z)
   %	Y: vodorovna osa s kladnym smerem doprava
   %	Z: vodorovna osa s kladnym smerem dopredu
   %
   %	rovina YZ tvori povrch detektoru, na ktery dopada svazek fotonu
   %	povrch detektoru lezi v X = 0
   
   %	ComptonC_rekonstrukce(10000,100,'nejistota',2,0)
   
  
   warning off MATLAB:MKDIR:DirectoryExists;   
   warning('off','MATLAB:dispatcher:InexactMatch') 
   close all; fclose all;
   
   paleta = 'default'; % 2D graf a sloupcovy graf (standardni barevna paleta, pouze pro 0 je zvolena bila barva)
   
   cd('./Data_test');
   %mkdir('Visualization');            % cd('Obrázky'); mkdir('TIF'); cd('..');
   
   %	==============  KONSTANTY  =====================
   sensor_size_half=0.7041;
   E_primary = 0.364;                 %energie primarniho fotonu [MeV] - pouze pro ELIST soubory, PTRAC soubory uz tuto informaci obsahuji
   pixel = 100*55E-6;                 % velikost pixelu v cm
   E_el = 0.510998910;                 % klidova hmotnost elektronu v MeV
   
   E_Cd = [22.957  23.153  26.065 26.614];                                      % X-ray cary Cd  mplib84
   E_Te = [27.187  27.465  30.974 31.681];                                      % X-ray cary Te  mplib84
   E_CdTe = [0.022 0.027];                                                      % udalosti s deponovanou energii v rozsahu <E_CdTe-sigma,E_CdTe+sigma> budou pricteny k jinym udalostem
   
   threshold  = 3.5E-3;               % minimalni deponovana energie v jedne udalosti, MeV
   dE         = 10E-3;                % plocha piku totalni absorpce bude spoctena v intervalu <Epik-dE;Epik+dE> MeV
   vzdMin     = 150*1E-4;             % minimalni vzdalenost 1. a 2. interakce interakci v rovine XY v cm
   pozice_unc = 1*[55 55 30]/1E4;     % nejistota stanoveni pozice interakce v cm; [u(x) u(y) u(z)]
   sigmaE     = 2*1E-3;               % nejistota stanoveni energie elektronu v 1 pixelu; MeV
   sigmaEtot_rel  = 0.04;                 % sigma rozmazani energie  = sigmaEtot_rel*deponovana_energie, v %/100
   %sigmaEtot  = 0.0045;                 % sigma rozmazani deponovane energie; MeV
   
   TH_edge = [   0    : 2:    180];   % deleni uhlu theta ve stupnich; hranice binu; deleni musi byt rovnomerne; 0° = kladny smer osy z = normala senzoru
   FI_edge = [-180    : 2:    180];   % deleni uhlu FI_cent ve stupnich; hranice binu; deleni musi byt rovnomerne; 0° = kladny smer osy x, 90° = kladny smer osy y
   add     = 45;                      % prida "add" stupnu na kazdou stranu TH_edge a FI_edge, aby bylo mozne spravne spocitat i gaussiany s maximem blizko hranic TH_edge a FI_edge; aplikuje se pouze v pripade gausovskeho rozmazavani
   
   %optional second sensor
   simulate_sensor2=0;
   sensor2_alpha=pi/4; %angle (around y-axis of the sensor) wrt the first sensor; sensors are touching each other and arranged in following way: \_
   
   %define Region Of Interest (ROI)
   roi_xmin=-6; %[cm]
   roi_xmax=-2; %[cm]
   roi_ymin=-2; %[cm]
   roi_ymax=2; %[cm]
   roi_zmin=-2; %[cm]
   roi_zmax=2; %[cm]
   ROI=[ roi_xmin roi_xmax; roi_ymin roi_ymax; roi_zmin roi_zmax];
   ROI_volume=abs((roi_xmax-roi_xmin)*(roi_ymax-roi_ymin)*(roi_zmax-roi_zmin))
   
   #3D reconstruction settings
   OE_nrand=100; %How many points on the surface of Compton cone do we want to create for each event? Only one point is accepted, however it has to lie within the region of interest (ROI). For small ROIs a larger number is therefore recommended.
   OE_ntrials=1; %How many times do we want to repeat the random number generation,if no point on the surface of Compton cone lies within the ROI?
   OE_cone_height=8; %height of the projection cone in cm, should be comparable with the size of the ROI and its distance from the detector
   %density_radius=1.0; %mm
   OE_density_distance=density_radius/10 %density_radius*(ROI_volume/nCoincidence)^(1/3) %[cm], when calculating event density in the vicinity of the reconfigured events, only events lying inside a sphere with this radius are counted 
   OE_MCMC_steps=MCMCsweeps*nCoincidence %number of Markov Chain MC steps
   %OE_MCMC_presteps=min([10*nCoincidence OE_MCMC_presteps*0.9]); %number of Markov Chain MC steps, which will not be included in the final averaged ensemble 
   OE_min_dist=0.2; %for improved x-resolution, use only events which are away from the sensor center
   OE_min_theta=pi/4; %for improved x-resolution, use only events with sufficiently large theta angle
   
   ans4=[1 2 3 4]; %which columns of the "store[]" array will be saved in addition to the ensemble matrix (they can be used to filter the final data) 
   
   %graphic output settings     
   %fig_path="./fig";
   bin_edges_xy={-6:0.1:-1 -3:0.1:3}; %binning for output histograms
   bin_edges_xz=bin_edges_xy; %binning for output histograms
   bin_edges_yz={-1:0.05:1 -1:0.05:1}; %binning for output histograms
   bin_nbins=[40 40]; %number of bins for automatically binned plots

     
   
   %	================================================
   
   display(sprintf(' '))
   display(sprintf('  ComptonC_rekonstrukce(A,B,''nejistoty'',C,D)'))
   display(sprintf('    A: kolik koincidenci bude pouzito pro rekonstrukci'))
   display(sprintf('    B: kolik sweepu ma mit MCMC retezec (nSweeps=nMCMCsteps/nEvents)'))
   display(sprintf('    ''nejistoty'': C=0/1 ... pri rekonstrukci nebudou/budou simulovany experimentalni nejistoty absorbovane energie '))
   display(sprintf('    ''nejistoty'': D=0/1 ... pri rekonstrukci nebudou/budou simulovany experimentalni nejistoty pozice interakce (u(x)=%1.1f um, u(y)=%1.1f um, u(z)=%1.1f um)',1e4*pozice_unc(1),1e4*pozice_unc(2),1e4*pozice_unc(3)))
   display(sprintf(' '))
   pause(0.2)
   
   %================================================
   %read input parameters
   %================================================
   doUncPosition=0; 
   doUncEnergy=0;
   doAcceptXsec=0;
   do12only=0;
   do21only=0;
   hweight=1.0;
   REP = 1; 
   %nastaveni simulovani nejistot
   [ano_S,index_S] = MCNP_varargin(varargin,'nejistoty');     % zkouma, jestli byl zadan parametr 'nejistoty', tedy zda se nebudou simulovat nejistoty detektorove rekonstrukce
   if ano_S == 1 && varargin{index_S+1} > 0 %rozmazat deponovanou energii energetickym rozlisenim detektoru
       doUncEnergy=varargin{index_S+1};
       display(sprintf('SIMULACE NEJISTOT: Deponovana energie bude rozmazana energetickym rozlisenim detektoru.'))
   endif
   if ano_S == 1 && varargin{index_S+2} > 0 %rozmazat polohu hitu v ramci pixelu (defaultne jsou vsechny clustery centrovany na stred pixelu)
       doUncPosition=varargin{index_S+2};
       display(sprintf('SIMULACE NEJISTOT:  Pozice clusteru bude rozmazana v ramci pixelu.'))
   endif 
   if ano_S == 0 || (varargin{index_S+1} == 0 && varargin{index_S+2} == 0)
       display(sprintf('SIMULACE NEJISTOT:  Nebude provedena.'))
   endif
   display(sprintf(' '))
   [ano_S,index_S] = MCNP_varargin(varargin,'xsec');     % zkouma, jestli byl zadan parametr 'xsec', ktery urcuje, zda se bude akceptovani novych MCMC kroku ridit i velikosti ucinneho prurezu; uplatnuje se pouze v pripade doUncEnergy==3
   if ano_S == 1 && varargin{index_S+1} > 0 && doUncEnergy==3 %rozmazat deponovanou energii energetickym rozlisenim detektoru
       doAcceptXsec=varargin{index_S+1};
       display(sprintf('Pri urcovani akceptance novych MCMC kroku bude pouzit ucinny prurez.'))
       display(sprintf(' '))
   endif
   [ano_S,index_S] = MCNP_varargin(varargin,'poradi');     % zkouma, jestli byl zadan parametr 'poradi', ktery urcuje, jake poradi interakci budeme brat v potaz (1-2, 2-1 nebo oboje)
   if ano_S == 1 && varargin{index_S+1} > 0  %rozmazat deponovanou energii energetickym rozlisenim detektoru
       do12only=2-varargin{index_S+1}; %poradi=1 -> true, poradi=2 -> false
       do21only=varargin{index_S+1}-1; %poradi=1 -> false, poradi=2 -> true
       display(sprintf('Bude vybrano pouze poradi udalosti %i - %i.',varargin{index_S+1},3-varargin{index_S+1}))
       display(sprintf(' '))
   endif
   
  [ano_S,index_S] = MCNP_varargin(varargin,'hweight');     % zkouma, jestli byl zadan parametr 'hweight', ktery nastavuje preferenci blizke (>1.0) ci vzdalene (<1.0) vzdalenosti pocatku (=zdroje fotonu) od bodu interakce s detektorem
   if ano_S == 1 && varargin{index_S+1} > 0 
       hweight=varargin{index_S+1};
       if hweight < 1.0
           display(sprintf('Pocatky budou generovany ve vetsi vzdalenosti.'))
       elseif hweight > 1.0
            display(sprintf('Pocatky budou generovany v mensi vzdalenosti.'))
       endif
       display(sprintf(' '))
   endif
   
        
   %================================================
   %NACITANI DAT
   %================================================

   seznamP0= MCNP_seznam('CC*.zip','soubor');        % najde vsechny soubory s vypisem historii, ktere byly vygenerovany skriptem "UTEF_ptrac_2019.m"
   seznamE = MCNP_seznam('*.elst','soubor');      % najde vsechny soubory formatu elist
   seznamP = [seznamP0; seznamE];
   
   if isempty(seznamP) == 1;
     error('     CHYBA: Nebyl nalezen žádný soubor "CC*.zip" s výpisem historií ani soubor formátu "elist" !!!');
   end
   
   for S = 1:size(seznamP,1)                                                   % pres vsechny soubory s výpisem historií
     close all
     dat1 = datestr(now,'yy-mm-dd_HH-MM-SS');    % date for file names
     dat2 = datestr(now,'dd.mm.yyyy HH:MM:SS');  % date for results
     cc = [0 0 0 0; 0 0 0 0];
     %			cc(1,1): =0/1 oprava na dvojne koincidence ne/provedena
     %			cc(2,1): =0/1 oprava na trojne koincidence ne/provedena
     %			cc(1,2): abs(cc(1,2)) - pocet oprav(dvojne koincidence -> jednoudalost)
     %			cc(2,2): cc(2,2)/2    - pocet oprav(trojne koincidence -> dvojne koincidence)
     %			cc(1,3): =0/1 oprava na nejmensi vzdalenost interakci ne/provedena
     %			cc(1,4): =1: PTRAC vystup; =0: format elist 
  
     [Data,vzdMin0,cc(1,4)] = READ_PTRAC_OR_ELIST(seznamP{S},S,size(seznamP,1),E_primary);               % nacte elist anebo vystup z PTRAC; cc(1,4) = 1: PTRAC vystup, cc(1,4) = 2: elist
     display(sprintf('File read.'))
      
    tic
     Data_zal = Data;    % zaloha originalniho Data
     
     %	========= omezeni vstupnich dat ========
     %		Data = Data(1:100,:)
     
     %	slouceni udalosti, ktere se vyskytly prilis blizko sebe na to, aby se daly odlisit (vzajemna vzdalenost je mensi nez "vzdMin" cm)
     [Data,cc(1,3)] = Merge_events(Data,vzdMin,cc);
     
     %	smazani samostatnych udalosti s deponovanou energii mensi nez threshold
     Data = Remove_events_below_threshold(Data,threshold);
     
     %	sloucit dvou a tri-eventove udalosti, kde jedna z energii odpovida energii fluorescencniho fotonu emitovaneho z materialu sensoru
     [Data,cc(1,1),cc(1,2)] = Merge_fluorescence(2, Data, E_CdTe(1)-sigmaE, E_CdTe(2)+sigmaE);    % slouci udalosti, kdy byl detekovan elektron s energii uvnitr intervalu <E-E1,E+E2>; 1) 2=dvojne koincidence, 3=trojne koincidence; 2) Data; 3) E1=minimalni energie; 4) E2=maximalni energie
     [Data,cc(2,1),cc(2,2)] = Merge_fluorescence(3, Data, E_CdTe(1)-sigmaE, E_CdTe(2)+sigmaE);    % NENI LEPSI (alespon pro CdTe+650 keV); slouci udalosti, kdy byl detekovan elektron s energii uvnitr intervalu <E-E1,E+E2>; 1) 2=dvojne koincidence, 3=trojne koincidence; 2) Data; 3) E1=minimalni energie; 4) E2=maximalni energie
     
     % zde jsou jiz data pripravena pro rekonstrukci
     %	 a nyni vyber dat pro rekonstrukci
          
   
     %	=========  1) Prave dve jakekoliv interakce primarniho fotonu ======
     P = find(Data(:,1)>0);                                                             % cisla radku odpovidajici prvni udalosti v historii
     %		w = find( (P(2:end)-P(1:end-1))==3   &   Data(P(1:end-1)+1,2)==1  &  Data(P(1:end-1)+2,2)==2  &  Data(P(1:end-1)+1,4)==2012 );
     w = find( (P(2:end)-P(1:end-1))==3 );   % ==1 - zadna interakce, ==2 - prave jedna interakce, ==3 - prave dve interakce
     
     %		vzd    = sqrt(sum((Data(P(w)+1,7:8)-Data(P(w)+2,7:8)).^2,2));                      % vzdalenost interakci v rovine XY v cm
     %		w(vzd < vzdMin) = [];                                                              % koincidence se vzdalenosti mensi nez limit nebudou uvazovany
     w = w(1:min([nCoincidence  length(w)]));                                      % pro rekonstrukci se vybere jen pozadovany pocet koincidenci
     
     nstore=12;                                                                       % we want to save only 12 data entries per event
     store1 = zeros(length(w),nstore);                                                       % priprava matice s poradim interakci 1-2
     store2 = 0*store1;                                                                 % priprava matice s opacnym poradim interakci 2-1
     %	store:  radky = jednotlive vybrane koincidence
     %			sloupce: 1.-3. - smerovy vektor osy kuzele (Vx,Vy,Vz)
     %					 4. - theta, uhel rozptylu fotonu v prvni interakci 
     %					 5. - cislo urcujici poradi interakci (toto neni pouzito)
     %					 6. - nejistota stanoveni uhlu z deponovane energie; v radianech; 
     %					 7. - jestli byla/nebyla otocena osa uhlu theta
     %					 8. - realisticka udalost
     %                   9. - dsigma/dOmega - Klein-Nishinuv ucinny prurez  
     %                   10-12. - 
     
     
     
     
     %	interakce '1-2' - spravne poradi
     %-------------------------------------
     E0=Data(P(w)+0,13); %E0 energie fotonu pred srazkou
     E1=Data(P(w)+1,15); %E1 deponovana energie pri prvni interakci (energie odrazeneho elektronu)
     E2=Data(P(w)+2,15); %E2 deponovana energie pri druhe interakci (energie odrazeneho elektronu)
     store1(:,1:3) = Data(P(w)+1,7:9)-Data(P(w)+2,7:9);                                           % vektor osy kuzele, presna pozice
     store1(:,  4) = acos(1 - (E_el./(E0.*((E0./E1) - 1))));   % uhel theta, uhel rozptylu fotonu: cos theta = 1 - m_e c^2 / [E0 (E0/E1 -1)]; E0 energie fotonu pred srazkou, E1 deponovana energie (energie odrazeneho elektronu)
     pomerE        = 1./(1+(E0/E_el).*(1 - cos(store1(:,4))));   %pomer energie fotonu pred interakci s detektorem a po 1. interakci (Comptonovu rozptylu), pomerE = E_in/E_out
     store1(:,  5) = Data(P(w)+1,7).^2+Data(P(w)+1,8).^2;
     store1(:,  6) = sigmaEtot_rel*E1.*(1 + E0/E_el .*(1-cos(store1(:,4)))).^2 ./ ((E0).^2/E_el .*sin(store1(:,4)));   % nejistota stanoveni uhlu z deponovane energie; v radianech; vztah (2.8) v Chapter 2: The Working Principles of a Compton Camera, str.21 [???], anebo https://doi.org/10.1016/S0168-9002(00)00669-0. 
     store1(:,  7) = 0;
     store1(:,  8) = 1;
     store1(:,  9) = 1*pomerE.^2 .*(pomerE + 1./pomerE - (sin(store1(:,4))).^2);                  % Klein-Nishinuv vztah;  https://en.wikipedia.org/wiki/Klein%E2%80%93Nishina_formula
     store1(:,  10:12) = Data(P(w)+1,7:9); %position of the first interaction
     
     %	interakce '2-1' - nespravne poradi
     %-------------------------------------
     store2(:,1:3) = -store1(:,1:3);                                                              % vektor osy kuzele
     store2(:,  4) = acos(1 - (E_el./(E0.*((E0./E2) - 1))));   % uhel theta, uhel rozptylu fotonu
     pomerE        = 1./(1+(E0/E_el).*(1 - cos(store2(:,4))));                       %pomer energie fotonu pred interakci s detektorem a po (dvojite) interakci, E_in/E_out
     store2(:,  5) = Data(P(w)+2,7).^2+Data(P(w)+2,8).^2;;
     store2(:,  6) = sigmaEtot_rel*E2.*(1 + E0/E_el .*(1-cos(store2(:,4)))).^2 ./ ((E0).^2/E_el .*sin(store2(:,4)));   % nejistota stanoveni uhlu z deponovane energie; v radianech; vztah (2.8) v Chapter 2: The Working Principles of a Compton Camera, str.21 [???], anebo https://doi.org/10.1016/S0168-9002(00)00669-0. 
     store2(:,  7) = 0;
     store2(:,  8) = 1;
     store2(:,  9) = 1*pomerE.^2 .*(pomerE + 1./pomerE - (sin(store2(:,4))).^2);                 % Klein-Nishinuv vztah;  https://en.wikipedia.org/wiki/Klein%E2%80%93Nishina_formula
     store2(:,  10:12) = Data(P(w)+2,7:9); %position of the second interaction (which will be treated as first one)
     
     time(1) = toc;      % cas pripravy dat
     tic                 % cas rekonstrukce
     

     %zkopirovat store1 a store2 REP-krat - abychom mohli vicekrat rozmazavat energii a polohu
    %if doUncEnergy >0 || doUncPosition > 0 
        store1 = repmat(store1,REP,1); 
        store2 = repmat(store2,REP,1);
        E0     = repmat(E0,REP,1);
    %endif
     
     %-------------------------------------
     % rozmazat mensi deponovanou energii, 
     % vetsi dopocitat z celkove energie primarniho fotonu a rozmazane mensi energie
     %-------------------------------------
     
     if doUncEnergy == 1
        %rozsireni REP-krat
        E1=repmat(E1,REP,1);
        E2=repmat(E2,REP,1);
        %rozmazani energii E1 a E2 relativni sigmou sigmaEtot_rel
        E1sigma=E1*sigmaEtot_rel; 
        E2sigma=E2*sigmaEtot_rel;
        E1s=normrnd(E1,E1sigma); %rozmazana E1 - z normalniho rozdeleni
        E2s=normrnd(E2,E2sigma); %rozmazana E1 - z normalniho rozdeleni
        w_E1gtE2=find(E1>E2); %vyber pripady s E1_recoil>E2_recoil
        w_E2gtE1=find(E1<=E2); %vyber pripady s E2_recoil>E1_recoil
        E1=E1s; %nahradit puvodni hodnotu rozmazanou hodnotou
        E2=E2s;
        E1(w_E1gtE2)=E0(w_E1gtE2)-E2s(w_E1gtE2);% E1>E2 => E2 nechame (ma mensi nejistotu), E1 dopocitame z E0
        E2(w_E2gtE1)=E0(w_E2gtE1)-E1s(w_E2gtE1);% E2>E1 => E1 nechame (ma mensi nejistotu), E2 dopocitame z E0
        store1(:,  4) = acos(1 - (E_el./(E0.*((E0./E1) - 1)))); %prepocitat uhel theta s rozmazanymi energiemi
        store2(:,  4) = acos(1 - (E_el./(E0.*((E0./E2) - 1)))); %prepocitat uhel theta s rozmazanymi energiemi
     endif
     
     %-------------------------------------
     % nejprve rozmazat E1 i E2, pak zpocitat nejpravdepodobnejsi E1 a E2
     %-------------------------------------
     if doUncEnergy == 2 || doUncEnergy == 3
        %rozsireni REP-krat
        E1=repmat(E1,REP,1);
        E2=repmat(E2,REP,1);
        %rozmazani energii E1 a E2 relativni sigmou sigmaEtot_rel
        E1sigma=E1*sigmaEtot_rel;
        E2sigma=E2*sigmaEtot_rel;
        E1s=normrnd(E1,E1sigma); %rozmazana E1 (z normalniho rozdeleni)
        E2s=normrnd(E2,E2sigma); %rozmazana E2 (z normalniho rozdeleni)
        
        w_E1gtE2=find(E1>E2); %vyber pripady s E1_recoil>E2_recoil
        w_E2gtE1=find(E1<=E2); %vyber pripady s E2_recoil>E1_recoil
        E1corr=E1s;
        E1corr(w_E1gtE2)=E0(w_E1gtE2)-E2s(w_E1gtE2);
     
        %odhad nejpravdepodobnejsi energie E1 (resp. E2 pro poradi 21)
        Ebest_mu1=(E1s.*(E2sigma.^2)+(E0-E2s).*(E1sigma.^2))./(E1sigma.^2+E2sigma.^2);
        Ebest_mu2=(E2s.*(E1sigma.^2)+(E0-E1s).*(E2sigma.^2))./(E1sigma.^2+E2sigma.^2);
        Ebest_sigma=sqrt(((E1sigma.^2).*(E2sigma.^2))./(E1sigma.^2+E2sigma.^2));
        Ebest1=normrnd(repmat(Ebest_mu1,1,MCMCsweeps), repmat(Ebest_sigma,1,MCMCsweeps));
        Ebest2=normrnd(repmat(Ebest_mu2,1,MCMCsweeps), repmat(Ebest_sigma,1,MCMCsweeps));
        store1(:,  4) = acos(1 - (E_el./(E0.*((E0./Ebest_mu1(:,1)) - 1)))); %prepocitat uhel theta s nejlepsim odhadem E1
        store2(:,  4) = acos(1 - (E_el./(E0.*((E0./Ebest_mu2(:,1)) - 1)))); %prepocitat uhel theta s nejlepsim odhadem E2
        
        Ediff_E1mu=(E1-Ebest_mu1)./E1*100;
        Ediff_E1E1s=(E1-E1s)./E1*100;
        Ediff_E1E1best=(E1-Ebest1(:,1))./E1*100;
        Ediff_E2mu=(E2-Ebest_mu2)./E2*100;
        Ediff_E2E2s=(E2-E2s)./E2*100;
        Ediff_E2E2best=(E2-Ebest2(:,1))./E2*100;
        
        Ediff_E1corr=(E1-E1corr)./E1*100;
        
     endif

     
     %-------------------------------------
     % aplikovat nejistotu polohy
     %-------------------------------------
     if doUncPosition == 1         
       store1(:,  10) =  repmat(Data(P(w)+1,7),REP,1)            .*(1 + pozice_unc(3)*randn(size(store1,1),1));
       store2(:,  10) =  repmat(Data(P(w)+2,7),REP,1)            .*(1 + pozice_unc(3)*randn(size(store2,1),1));
       store1(:,  11) =  pixel*(ceil(repmat(Data(P(w)+1,8),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1));
       store2(:,  11) =  pixel*(ceil(repmat(Data(P(w)+2,8),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store2,1),1) - 1));
       store1(:,  12) =  pixel*(ceil(repmat(Data(P(w)+1,9),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1));
       store2(:,  12) =  pixel*(ceil(repmat(Data(P(w)+2,9),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store2,1),1) - 1));
       
       store1(:,  1) = store1(:,  10) - store2(:,  10);
       store1(:,  2) = store1(:,  11) - store2(:,  11);
       store1(:,  3) = store1(:,  12) - store2(:,  12);
       
       store2(:,1:3) = -store1(:,1:3);
        
        %store1(:,  1) =  pixel*(ceil(repmat(Data(P(w)+1,7),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1))  -  pixel*(ceil(repmat(Data(P(w)+2,7),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1)) ;   % x-ova slozka vektoru osy kuzele, na stred pixelu + nahodne cislo s rovnomernym rozdelenim uvnitr pixelu
       %store1(:,  2) =  pixel*(ceil(repmat(Data(P(w)+1,8),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1))  -  pixel*(ceil(repmat(Data(P(w)+2,8),REP,1)/pixel)-0.5).*(1 + pixel/2*(2*rand(size(store1,1),1) - 1)) ;   % y-ova slozka vektoru osy kuzele, na stred pixelu + nahodne cislo s rovnomernym rozdelenim uvnitr pixelu
       %store1(:,  3) =              repmat(Data(P(w)+1,9),REP,1)            .*(1 + pozice_unc(3)*randn(size(store1,1),1))   -              repmat(Data(P(w)+2,9),REP,1)            .*(1 + pozice_unc(3)*randn(size(store1,1),1));     % z-ova slozka vektoru osy kuzele, s gaussovym rozlozenim okolo presne hodnoty se sigma = nejistota stanoveni z-ove polohy
       %store2(:,1:3) = -store1(:,1:3);                                                                                              % vektor osy kuzele, na stred pixelu
     endif
     if doUncPosition == 2
       store1(:,  10) =  repmat(Data(P(w)+1,7),REP,1)            .*(1 + pozice_unc(3)*randn(size(store1,1),1));
       store2(:,  10) =  repmat(Data(P(w)+2,7),REP,1)            .*(1 + pozice_unc(3)*randn(size(store2,1),1));
       %pouze posuneme na stred pixelu
       store1(:,  11) =  pixel*(ceil(repmat(Data(P(w)+1,8),REP,1)/pixel)-0.5);
       store2(:,  11) =  pixel*(ceil(repmat(Data(P(w)+2,8),REP,1)/pixel)-0.5);
       store1(:,  12) =  pixel*(ceil(repmat(Data(P(w)+1,9),REP,1)/pixel)-0.5);
       store2(:,  12) =  pixel*(ceil(repmat(Data(P(w)+2,9),REP,1)/pixel)-0.5);
       
       store1(:,  1) = store1(:,  10) - store2(:,  10);
       store1(:,  2) = store1(:,  11) - store2(:,  11);
       store1(:,  3) = store1(:,  12) - store2(:,  12);
       
       store2(:,1:3) = -store1(:,1:3);
     endif

     %--------------------------------------
     % presunout polovinu udalosti do druheho detektoru
     %-------------------------------------
     if simulate_sensor2 == 1
        s2sel=[2:2:length(w)]; %pick every 2nd event
        store1(s2sel,10) = -sensor_size_half*sin(sensor2_alpha) + sin(sensor2_alpha)*store1(s2sel,11); %11 at the end is not a typo, it is correct
        store2(s2sel,10) = -sensor_size_half*sin(sensor2_alpha) + sin(sensor2_alpha)*store2(s2sel,11); 
        store1(s2sel,11) = -sensor_size_half-store1(s2sel,11) - sensor_size_half*cos(sensor2_alpha) + cos(sensor2_alpha).*store1(s2sel,11) ;
        store2(s2sel,11) = -sensor_size_half-store2(s2sel,11) - sensor_size_half*cos(sensor2_alpha) + cos(sensor2_alpha).*store2(s2sel,11) ;
     
        %we will also rotate the cone axis by sensor2_alpha
        x1_old=store1(s2sel,1);
        y1_old=store1(s2sel,2);
        %x2_old=store1(s2sel,1);
        %y2_old=store1(s2sel,2);
        store1(s2sel,1)=cos(sensor2_alpha)*x1_old-sin(sensor2_alpha)*y1_old;
        store1(s2sel,2)=sin(sensor2_alpha)*x1_old+cos(sensor2_alpha)*y1_old;
        %store2(s2sel,1)=cos(sensor2_alpha)*x2_old-sin(sensor2_alpha)*y2_old;
        %store2(s2sel,2)=sin(sensor2_alpha)*x2_old+cos(sensor2_alpha)*y2_old;
        store2(:,1:3) = -store1(:,1:3); 
     endif
     
     %-------------------------------------
     %	slouceni, vylouceni nerealnych udalosti, uprava
     %-------------------------------------
     if do12only   %bereme pouze poradi interakce 1-2 (vhodne u double-layer detektoru Si+CdTe)
        store  = store1;
        E00=E0;
        if doUncEnergy == 2 || doUncEnergy == 3
            Ebest = Ebest1;
            EbestNo = ones(REP*length(w),1);
            EbestMu = Ebest_mu1;
        endif
     elseif do21only %bereme pouze poradi interakce 2-1
        store  = store2;
        E00=E0;
        if doUncEnergy == 2 || doUncEnergy == 3
            Ebest = Ebest2;
            EbestNo = ones(REP*length(w),1);
            EbestMu = Ebest_mu2;
        endif
     else     %bereme poradi interakce 1-2 i 2-1, protoze nevime, co nastalo drive (vhodne pro single-layer detektor)
        store  = zeros(2*REP*length(w),nstore);                                               % priprava smichane matice REP * (store1 + store2)
        store(2:2:2*REP*length(w),  :) = store2;
        store(1:2:2*REP*length(w)-1,:) = store1;
        E00(2:2:2*REP*length(w),  :) = E0;
        E00(1:2:2*REP*length(w)-1,:) = E0;
        
        if doUncEnergy == 2 || doUncEnergy == 3
            Ebest = zeros(2*REP*length(w),MCMCsweeps);
            Ebest(2:2:2*REP*length(w),  :) = Ebest2;
            Ebest(1:2:2*REP*length(w)-1,:) = Ebest1;
            EbestNo = ones(2*REP*length(w),1);
            EbestMu = zeros(2*REP*length(w),1);
            EbestMu(2:2:2*REP*length(w),  1) = Ebest_mu2;
            EbestMu(1:2:2*REP*length(w)-1,1) = Ebest_mu1;
        endif
     endif
        
     % spravne a spatne poradi interakci se musi v souhrnnem vypisu vzajemne stridat, aby se pri rekonstrukci z predchozich X koincidenci v prvni casti nerekonstruovaly jen spravne smery a ve druhe casti jen spatne smery
     %		store = store(1:2:2*REP*length(w)-1,:);          % vybrani jen spravneho poradi interakci
     %		store = store(2:2:2*REP*length(w)  ,:);          % vybrani jen spatneho poradi interakci
     store(imag(store(:,4)) ~= 0,8) =  0;                                              % nerealisticky uhel   
     store(imag(store(:,6)) ~= 0,8) =  0;                                              % nerealisticka energie   
     store(store(:,4) > pi/2,1:3)   =   -store(store(:,4) > pi/2, 1:3); % pokud je vrcholovy uhel > 90°, potom se kuzel otoci - vektor osy je opacny...
     store(store(:,4) > pi/2,7)   =   1;                                % a ulozime informaci, ze jsme uhel otocili
     store(store(:,4) > pi/2,  4)   = pi-store(store(:,4) > pi/2,   4);                 % ...a vrcholovy uhel je doplnek do 180°;  cos(pi-a) = -cos(a)
     store(:,1:3)                   = UNIT_VECTOR(store(:,1:3),2);                      % prevod na jednotkovy vektor

       
    
     %============================================================
     %         3D rekonstrukce
     %============================================================
     %Origin Ensembles 3D reconstruction - procedure described in 
     %Med Phys. 2011 Jan;38(1):429-38. doi: 10.1118/1.3528170.
     display(sprintf('Reconstruction in progress:'))
    Config=zeros(size(store,1),4); %one particular configuration of possible solutions, row: (x,y,z,flag)
    theta=store(:,4); %half of the apex angle of the projection cone
    crossX=store(:,9);

     %initial configuration of events - first we will create one particular configuration of events, which will be then randomly changed during the MCMC loop, one event per one step of the MCMC loop
     nevents=size(store,1);
     for u = 1:nevents
        if store(u,8)==0 %skip events with unrealistic energy/angle
            continue;
        endif
        vaxis=store(u,1:3); %axis of the projection cone
        pixel_position=store(u,10:12); %position of the first interaction
        [xyz,flag] = point_on_cone(vaxis,theta(u),OE_cone_height,pixel_position,ROI,OE_nrand,OE_ntrials,hweight);
        Config(u,1:3) = xyz; %x,y,z coordinate of a point on the Compton cone, lying in the ROI
        Config(u,4) = flag; %flag=1: good solution, flag=0: no solution found
        
        if doUncEnergy == 2 || doUncEnergy == 3 %calculate Klein-Nishina cross section for corrected theta
            ratioE=1/(1+(E00(u)/E_el)*(1 - cos(theta(u))));  
            crossX(u)=1*ratioE^2 *(ratioE + 1/ratioE - (sin(theta(u)))^2); 
        endif
     endfor
     good_evts=find(Config(:,4)>0);
     Ensemble=[Config(good_evts,1:3) store(good_evts,ans4)]; %ensemble of all accepted configurations - it will be extended in each step of MCMC chain
     
   
     %Markov Chain Monte Carlo (MCMC) loop
     for ch = 1:OE_MCMC_steps
        percent= floor(ch/(OE_MCMC_steps/100));
        remind=mod(ch,floor(OE_MCMC_steps/10));
        
        if  remind == 0
            display(sprintf('%i %%',percent))
        endif
        evt=ceil(rand(1,1)*nevents); %pick up a random event
        Config_new=Config;
        crossX_new=crossX(evt);
       if store(evt,8)==0 %skip events with unrealistic energy/angle
            continue;
        endif
       vaxis=store(evt,1:3);
       if doUncEnergy == 2 || doUncEnergy == 3
            if doUncEnergy == 2
                theta(evt) = acos(1 - (E_el/(E00(1)*((E00(1)/EbestMu(evt)) - 1)))); %use mean best value EbestMu
            else
                theta(evt) = acos(1 - (E_el./(E00(evt).*((E00(evt)./Ebest(evt,EbestNo(evt))) - 1)))); %use best value sampled from normal distribution with mu= EbestMu
            endif
            
            if (imag(theta(evt)) ~= 0)  %nerealisticky uhel
                continue;
            endif
           if theta(evt) > pi/2
               theta(evt) = pi - theta(evt);
               if store(evt,7)==0 %puvodni uhel nebyl >pi/2 a proto osa uhlu jeste nebyla otocena -> otocime ji nyni
                    vaxis=-vaxis;
                endif
           endif %theta(evt) > pi/2
             
           ratioE=1/(1+(E00(u)/E_el)*(1 - cos(theta(evt))));  
           if doAcceptXsec == 1 
                crossX_new=1*ratioE^2 *(ratioE + 1/ratioE - (sin(theta(evt)))^2);
           endif
           %theta_diff=theta(evt)-store(evt,4);
           EbestNo(evt)=EbestNo(evt)+1; %pro jednu a tu samou udalost evt pouzijeme v kazdem dalsim MCMC kroku jine nahodne cislo 
           if EbestNo(evt) > MCMCsweeps %reset the counter once we reach the size of the Ebest array
               EbestNo(evt) = 1;
           endif
       endif % doUncEnergy == 2 || doUncEnergy == 3
       pixel_position=store(evt,10:12); %position of the first interaction
       [xyz_new,flag_new] = point_on_cone(vaxis,theta(evt),OE_cone_height,pixel_position,ROI,OE_nrand,OE_ntrials,hweight);
       if flag_new==0 %no solution found 
         continue;
       endif

       flag_old=Config(evt,4);
       xyz_old=Config(evt,1:3);
       if flag_old==0 % the old event is invalid => accept the new event immediately
         Config(evt,1:3) = xyz_new; 
         Config(evt,4) = flag_new; 
       else
         Config_new(evt,1:3) = xyz_new; 
         Config_new(evt,4) = flag_new; 
         
         %calculate event density in the vicinity of the old event and the new event
         points_new=Config_new(Config_new(:,4)>0,1:3); %take only valid points, with flag>0
         points_old=Config(Config(:,4)>0,1:3); %take only valid points, with flag>0
         density_new=event_density(xyz_new, OE_density_distance, points_new);
         density_old=event_density(xyz_old, OE_density_distance, points_old);
         
         %calculate acceptance probability - the new event will be accepted only with this probability
         rand(1,1);
         ratio=density_new*crossX_new/(density_old*crossX(evt)); % the cited paper states ratio=(density_new+1/density_old) but I guess it is a typo (?)
         acc_prob=min(1, ratio);
         if rand < acc_prob % accept new event (otherwise the old event will be added to the ensemble once again)
           Config=Config_new;
         endif
         new_line=[Config(evt,1:3) store(evt,ans4)];
         Ensemble=[Ensemble; new_line];%add only the actual event to the ensemble (not the whole configuration - this is the main difference when compared to the original article) 

         %if ch > OE_MCMC_presteps %add the new configuration to the final ensemble
             %Ensemble(:,1:3)=Ensemble(:,1:3)+Config(:,1:3);
             %%Ensemble(evt,4)=Ensemble(evt,4)+1; %for each point record the number of iterations it has undergone
             %Ensemble(evt,4)=Ensemble(evt,4)+density_new; %for each point record the number of points in its surrounding
             %nConfigs=nConfigs+Config(:,4); %we count only good events (flag=1) (however all newly created events are already good, so ncConfigs+1 would do the job as well)
             
             %Ensemble=[Ensemble;Config(Config(:,4)>0,:)]; %add only good events from the new configuration to the ensemble           
       endif %old event is invalid/good
       
     endfor %end of MCMC loop
   
      %===================================
     %	konec rekonstrukce, nyni publikace vysledku
     %===================================
     fig_desc=sprintf("dmm%.1f_evts%i_MCsteps%ik_uE%i%i_uPos%i_pair%i%i_hw%.2f",density_radius,nCoincidence,OE_MCMC_steps/1000,doUncEnergy,doAcceptXsec,doUncPosition,do12only,do21only,hweight);
     fig_ext="png";
     
     time(2) = toc;
     display(sprintf('  Reconstruction time:  %f s',sum(time)))
     
     %hustota v okoli zdroje v zavislosti na poctu sweeps - pro urceni konvergence MCMC
     source_position=[-4 0 0];
     ans_size=size(Ensemble,1);
     maxsweeps=ans_size/nCoincidence; %pocet sweepu spocitame pouze z prijatych udalosti, nikoliv vygenerovanych
     sweeps2plot=[min(100,floor(maxsweeps/10)):min(100,floor(maxsweeps/10)):maxsweeps]; %plot only every 100th sweep
     nplotsweeps=size(sweeps2plot,2);
     if nplotsweeps<1
         nplotsweeps=1;
         sweeps2plot=1;
     endif
     density_source=zeros(1,nplotsweeps);
     for sw = 1:nplotsweeps
         nparts=sweeps2plot(1,sw); %normalize the density by the number of generated origins 
         density_source(1,sw)=event_density(source_position, OE_density_distance, Ensemble(1:nparts,1:3))*1000/nparts; %1000 is here just to get a number closer to 1
     endfor

     %vypis zakladnich udaju o rekonstrukci
     fileID = fopen(sprintf('../%s/stats_%s.txt',fig_path,fig_desc),'a');
     fprintf (fileID,"Number of events:  %i \n",nCoincidence);
     fprintf (fileID,"Number of MCMC sweeps:  %i \n",MCMCsweeps);
     fprintf (fileID,"density radius:  %.1f mm \n",OE_density_distance*10);
     fprintf (fileID,"density around source:  %.3f \n",density_source(1,end));
     fprintf(fileID,'Reconstruction time:  %f s \n\n',sum(time));
     fclose(fileID);
     
     %******************************
     %plot results
     %******************************
   
     %3D scatter plot
     sample=MCMCsweeps/10; %plot only every sample^th point in 3D plot 
     Ensemble_sub=Ensemble(1:sample:end,:);
     max_color=max(Ensemble_sub(:,4));
     min_color=min(Ensemble_sub(:,4));
     color_ratio=(Ensemble_sub(:,4)-min_color)/(1+max_color-min_color);
     colorv=[color_ratio,0*color_ratio,1-color_ratio];
     figure(1);
     scatter3 (Ensemble_sub(:,1), Ensemble_sub(:,2), Ensemble_sub(:,3), 10, colorv, "o");
     xlabel ("x");
     ylabel ("y");
     zlabel ("z");
     fig_name="scatter3D";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(2);
     subplot(2,1,1);
     [counts1, centers1] = hist3 (Ensemble(:,[1 2]),"Edges",bin_edges_xy);
     counts2=counts1./MCMCsweeps;
     imagesc (centers1{1}, centers1{2}, counts2');
     colorbar();
     title("x-y projection");
     xlabel ("x [cm]");
     ylabel ("y [cm]");

     subplot(2,1,2);
     [rowMax, colMax] = find(ismember(counts2, max(counts2(:))));
     binWidth=(bin_edges_xy{1}(2)-bin_edges_xy{1}(1))/2;
     h_axis=bin_edges_xy{1}+binWidth;
     h_data=counts2(:,colMax(1))';
     bar(h_axis,h_data);
     title("slice at y_{max}~0");
     xlim ([-6 0]);
     xlabel ("x [cm]");
     
     fig_name="xy";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(3);
     subplot(2,1,1);
     [counts1, centers1] = hist3 (Ensemble(:,[1 3]),"Edges",bin_edges_xz);
     counts2=counts1./MCMCsweeps;
     imagesc (centers1{1}, centers1{2}, counts2');
     colorbar();
     title("x-z projection");
     xlabel ("x [cm]");
     ylabel ("z [cm]");

     subplot(2,1,2);
     [rowMax, colMax] = find(ismember(counts2, max(counts2(:))));
     binWidth=(bin_edges_xz{1}(2)-bin_edges_xz{1}(1))/2;
     h_axis=bin_edges_xz{1}+binWidth;
     h_data=counts2(:,colMax(1))';
     bar(h_axis,h_data);
     title("slice at z_{max}~0");
     xlim ([-6 0]);
     xlabel ("x [cm]");
     
     fig_name="xz";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(4);
     subplot(2,1,1);
     [counts1, centers1] = hist3 (Ensemble(:,[2 3]),"Edges",bin_edges_yz);
     counts2=counts1./MCMCsweeps;
     imagesc (centers1{1}, centers1{2}, counts2');
     colorbar();
     title("y-z projection");
     xlabel ("y [cm]");
     ylabel ("z [cm]");

     subplot(2,1,2);
     [rowMax, colMax] = find(ismember(counts2, max(counts2(:))));
     binWidth=(bin_edges_yz{1}(2)-bin_edges_yz{1}(1))/2;
     h_axis=bin_edges_yz{1}+binWidth;
     h_data=counts2(:,colMax(1))';
     bar(h_axis,h_data);
     title("slice at z_{max}~0");
     xlim ([-1 1.5]);
     xlabel ("y [cm]");
     
     fig_name="yz";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(5);
     plot(sweeps2plot,density_source);
     ylabel ("density around source [arb. units]");
     xlabel ("number of sweeps");
     fig_name="density";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     
  
     if doUncEnergy == 2 || doUncEnergy == 3
     
        markerSize=5;
        sample=1; %nCoincidence/100; % plot only every sample^th step
     
        figure(6);
        subplot(2,2,1);
        scatter(Ediff_E1E1s(1:sample:end),Ediff_E2E2s(1:sample:end),markerSize);
        title("energy smearing");
        xlabel ("E [keV]");
        ylabel ("E [keV]");
     
        subplot(2,2,2);
        scatter(Ediff_E1mu(1:sample:end),Ediff_E2mu(1:sample:end),markerSize);
        title("Etrue - mean Ebest");
        xlabel ("E [keV]");
        ylabel ("E [keV]");
        
        subplot(2,2,3);
        scatter(Ediff_E1E1best(1:sample:end),Ediff_E2E2best(1:sample:end),markerSize);
        title("Etrue - Ebest");
        xlabel ("E [keV]");
        ylabel ("E [keV]");
        
        %subplot(2,2,4);
        %scatter(Ediff_E1mu(1:sample:end),Ediff_E1corr(1:sample:end),markerSize);
        %title("Etrue - Ebest");
        %xlabel ("#Delta_{best} [keV]");
        %ylabel ("#Delta_{corr} [keV]");
        
        subplot(2,2,4);
        scatter(store(1:sample:end,4),theta(1:sample:end),markerSize);
        title("#theta_{true} vs #theta_{best}");
        xlabel ("#theta_{true} [rad]");
        ylabel ("#theta_{best} [rad]");
           
        fig_name="Esmear2";
        filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
        print(filename, "-dpng");
     endif
     
     figure(7);
     subp=0;
     for ev = floor(ans_size/10):floor(ans_size/10):ans_size
        subp=subp+1;
        if subp>9
            continue;
        endif
        swp=floor(ev/nCoincidence);
        subplot(3,3,subp);
        [counts1, centers1] = hist3 (Ensemble(1:ev,[1 2]),"Edges",bin_edges_xz);
        counts2=counts1./MCMCsweeps;
        imagesc (centers1{1}, centers1{2}, counts2');
        colorbar();
        title(sprintf("x-z projection, %i sweeps",swp));
        xlabel ("x [cm]");
        ylabel ("y [cm]");
     endfor
     fig_name="xy_sweeps";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(8);
     
     scatter3 (store(:,10), store(:,11), store(:,12), 10, "r", "o");
     axis ([-sensor_size_half sensor_size_half -sensor_size_half sensor_size_half -sensor_size_half sensor_size_half]);
     xlabel ("x");
     ylabel ("y");
     zlabel ("z");
     fig_name="hits3D";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
     figure(9); %x vs distance between 1st and 2nd hit in x and in yz plane
     bin_edges={-6:0.1:-1 -1.2:0.1:1.2}; %binning
     subplot(2,1,1); %distance in x
     [counts1, centers1] = hist3 (Ensemble(:,[1 4]),"Edges",bin_edges);
     counts2=counts1./MCMCsweeps;
     imagesc (centers1{1}, centers1{2}, counts2');
     colorbar();
     title("x vs deltaX");
     xlabel ("x [cm]");
     ylabel ("deltaX [cm]");
     
     bin_edges={-6:0.1:-1 0:0.05:1.5}; %binning
     pdata=sqrt(Ensemble(:,5).^2+Ensemble(:,6).^2); %distance in yz plane
     pdata=[Ensemble(:,1) pdata(:)];
     subplot(2,1,2); 
     [counts1, centers1] = hist3 (pdata,"Edges",bin_edges);
     counts2=counts1./MCMCsweeps;
     imagesc (centers1{1}, centers1{2}, counts2');
     colorbar();
     title("x vs deltaYZ");
     xlabel ("x [cm]");
     ylabel ("deltaYZ [cm]");
     
     fig_name="xVSpairDist";
     filename=sprintf("../%s/%s_%s.%s",fig_path,fig_name,fig_desc,fig_ext);
     print(filename, "-dpng");
     
   endfor %loop over input files
 endfunction
 
  
 %================================================================================
 %                              ADDITIONAL FUNCTIONS
 %================================================================================
 
 function a = UNIT_VECTOR(a,dim)
   %	spocita jednotkovy vektor
   %	a - matice NxM s N vektory delky M, vektory = radky
   %	dim - podle jake dimenze je v matici uveden vektor: dim = 1: vektory jsou ve sloupcich; dim = 2: vektory jsou v radcich
   
   if dim == 1
     a = a./repmat(sqrt(sum(a.^2,1)),size(a,1),1); 
   elseif dim == 2
     a = a./repmat(sqrt(sum(a.^2,2)),1,size(a,2)); 
   end
   
 endfunction
 
%================================================================================

 
 function Data = Remove_events_below_threshold(Data,threshold);
   
   Data(Data(:,1) < 0  &  Data(:,15) < threshold,:) = [];
   
 endfunction
 
  
 %================================================================================
 
 
 function [Data,c] = Merge_events(Data,vzdMin,cc)
   %	slouceni udalosti, ktere se vyskytly prilis blizko sebe na to, aby se daly odlisit (vzajemna vzdalenost je mensi nez "vzdMin" cm)
   
   delRow = [];                                                                % vektor se radky "Data", ktere budou smazany
   c = 0;
   
   if cc(1,4) == 1                                                             % jen PTRAC vystup
     c  = 1;                                                                 % index, ze byla provedena oprava na vzdalenost
     P  = find(Data(:,1)>0);                                                 % cisla radku odpovidajici prvni udalosti v historii
     MX = max(P(2:end)-P(1:end-1)) -1;                                       % nejvyssi pocet udalosti v jedne historii
     
     for p = 2:MX                                                            % pres vsechny pocty udalosti
       w = find( (P(2:end)-P(1:end-1)) == p+1 );                           % najde historie s danym poctem udalosti
       for A = p:-1:2                                                      % od posledni udalosti ke druhe                    
         for B = A-1:-1:1                                                % od predposledni udalosti k prvni
           %					[A B length(w)]
           [delRow,Data] = Najdi_vzdalenost(w,A,B,P,Data,delRow,vzdMin); % najde vzdalenost mezi A-tou a B-tou udalosti a pokud je mensi nez vzdMin, tak je slouci
         end
       end
     end
   end
   
   Data(delRow,:) = [];
   
 endfunction

 
 %================================================================================
  
  
 function [delRow,Data] = Najdi_vzdalenost(w,A,B,P,Data,delRow,vzdMin)
   %	slouceni udalosti, ktere se vyskytly prilis blizko sebe na to, aby se daly odlisit (vzajemna vzdalenost je mensi nez "vzdMin" cm)
   
   %	vzd = sum([max([ zeros(length(w),1) -vzdMin+min([ sqrt(sum((Data(P(w)+A,17:18)-Data(P(w)+B,17:18)).^2,2))  sqrt(sum((Data(P(w)+A,17:18)-Data(P(w)+B,20:21)).^2,2))  sqrt(sum((Data(P(w)+A,20:21)-Data(P(w)+B,17:18)).^2,2))  sqrt(sum((Data(P(w)+A,20:21)-Data(P(w)+B,20:21)).^2,2))], [],2)], [],2)    abs(Data(P(w)+A,6)-Data(P(w)+B,6))],2);       % 1.hodnota: 0 kdyz alespon jedna vzdalenost je mensi nez vzdMin, jinak minimalni vzdalenost; 2.hodnota: 0 kdyz jsou obe udalosti ve stejne bunce, jinak absolutni hodnota jejich rozdilu
   vzd = sum([max([ zeros(length(w),1) -vzdMin+min([ VZDALENOST(Data(P(w)+A,17:19),Data(P(w)+B,17:19),Data(P(w)+A,6),Data(P(w)+B,6))   VZDALENOST(Data(P(w)+A,17:19),Data(P(w)+B,20:22),Data(P(w)+A,6),Data(P(w)+B,6))   VZDALENOST(Data(P(w)+A,20:22),Data(P(w)+B,17:19),Data(P(w)+A,6),Data(P(w)+B,6))   VZDALENOST(Data(P(w)+A,20:22),Data(P(w)+B,20:22),Data(P(w)+A,6),Data(P(w)+B,6))], [],2)], [],2)    abs(Data(P(w)+A,6)-Data(P(w)+B,6))],2);       % 1.hodnota: 0 kdyz alespon jedna vzdalenost je mensi nez vzdMin, jinak minimalni vzdalenost; 2.hodnota: 0 kdyz jsou obe udalosti ve stejne bunce, jinak absolutni hodnota jejich rozdilu
   %		kdyz je vzd=0, potom se dane dve udalosti slouci, a u sloucene udalosti budou zmeneny nektere parametry: 
   
   Pv = P(w(vzd==0));
   v1 = intersect(Pv+B, find(Data(:,2)>0)); 
   Data(v1,[4 10:12 13]) = Data(v1-B+A,[4 10:12 13]);                                                                % jen interakce primarniho fotonu: ID interakce, U,V,W rozptyleneho fotonu, energie fotonu po interakci
   
   Data(Pv+B,[14 15 29:30 32 34]) =       Data(Pv+B,[14 15 29:30 32 34]) + Data(Pv+A,[14 15 29:30 32 34]);           % interakce jakehokoliv fotonu: pocatecni energie odrazeneho elektronu, energie deponovana v senzoru timto elektronem, pocty a energie Augerovych elektronu, energie fluorescencnich a X-ray fotonu
   Data(Pv+B,[16               ]) = max([ Data(Pv+B,[16               ]) + Data(Pv+A,[16               ])],[], 2);   % interakce jakehokoliv fotonu: suma energie odrazeneho fotonu + pocatecni energie elektronu
   Data(Pv+B,[27               ]) = max([ Data(Pv+B,[27               ]) + Data(Pv+A,[27               ])],[], 2);   % interakce jakehokoliv fotonu: vzdalenost dvou interakci primarniho fotonu
   Data(Pv+B,[26               ]) = max([zeros(length(w(vzd==0)),1) Data(Pv+B,[26])],[],2) + max([zeros(length(w(vzd==0)),1) Data(Pv+A,[26])],[],2);         % interakce jakehokoliv fotonu: suma delek odrazenych elektronu
   
   
   v2 = intersect(1:length(Pv), find(Data(Pv+B,4) == 2011  &  Data(Pv+B,32)>Data(Pv+A,14)  &  Data(Pv+B,31) >= 1));  % fotoefekt libovolneho fotonu a fluorescencni foton neinteragoval blizko mista sveho vzniku
   %	Pv(v2)
   v3 = setdiff(  1:length(Pv), v2);                                                                                 % ...ostatni pripady                           
   Data(Pv(v2)+B,31) = Data(Pv(v2)+B,31) - (1-1/100)*floor(Data(Pv(v2)+B,31)) + Data(Pv(v2)+A,31);                   % fotoefekt libovolneho fotonu a fluorescencni foton neinteragoval blizko mista sveho vzniku
   Data(Pv(v3)+B,31) = Data(Pv(v3)+B,31)                                      + Data(Pv(v3)+A,31);                   % ...ostatni pripady
   
   
   v4 = intersect(1:length(Pv), find(Data(Pv+B,34)>Data(Pv+A,14)  &  Data(Pv+B,33) >= 1));                           % pokud je energie X-ray fotonu generovaneho drivejsim elektronem > deponovana energie pozdejsiho elektronu, potom se pozdejsi elektron povazuje jako vznikly X-ray fotonem vzniklym z drivejsiho elektronu
   %	Pv(v4)
   v5 = setdiff(  1:length(Pv), v4);                                                                                 % ...ostatni pripady                           
   Data(Pv(v4)+B,33) = Data(Pv(v4)+B,33) - (1-1/100)*floor(Data(Pv(v4)+B,33)) + Data(Pv(v4)+A,33);                   % pokud je energie X-ray fotonu generovaneho drivejsim elektronem > deponovana energie pozdejsiho elektronu, potom se pozdejsi elektron povazuje jako vznikly X-ray fotonem vzniklym z drivejsiho elektronu
   Data(Pv(v5)+B,33) = Data(Pv(v5)+B,33)                                      + Data(Pv(v5)+A,33);                   % ostatni pripady
   
   
   delRow = [delRow; P(w(vzd==0))+A];      % tyto radky budou z "Data" vymazany
   Data(P(w(vzd==0))+A,6) = 1E8;           % prictena udalost jiz nebude uvazovana v nasledujicich pricitacich for cyklech (resp. cislo bunky je pred smazanim celeho radku zmeneno na takove cislo, ze uz nikdy tato udalost v aktualnim for cyklu nebude vybrana)
   
 endfunction
 
 
  %================================================================================

 
 function vzd = VZDALENOST(vec1,vec2,cel1,cel2)     % vzd = VZDALENOST(vec1,vec2,cel1,cel2)   sqrt(x^2 + y^2 + (bunka1-bunka2)*z^2); pokud je cislo bunek obou interakci stejne, potom se z-ova slozka neuvazuje. V opacnem pripade se uvazuje
   
   vzd = sqrt( (vec1(:,1)-vec2(:,1)).^2 + (vec1(:,2)-vec2(:,2)).^2 + min([ones(size(vec1,1),1) abs(cel1-cel2)],[],2).*(vec1(:,3)-vec2(:,3)).^2 );  % vzdalenost interakci v cm: v rovine XY pokud interakce ve stejne bunce, anebo v xyz pokud interakce v ruznych bunkach
   
 endfunction
 
 
  %================================================================================

 
 
 function  [Data0,cc,L2] = Merge_fluorescence(p, Data, Emin, Emax)
   % slouci udalosti, kdy byl detekovan elektron s energii uvnitr intervalu <E-E1,E+E2>
   %	1) 2=dvojne koincidence, 3=trojne koincidence
   %	2) Data
   %	3) Emin=minimalni energie
   %	4) Emax=maximalni energie
   
   
   
   L1 = size(Data,1);
   
   % 	Data(500:end,:) = [];
   % 	Data(:,[1 2 3 15])
   cc = 1;
   for A = 1:p
     P   = find(  Data(:,1) > 0);                                                                                  % cisla radku odpovidajici prvni udalosti v historii
     w   = find( (P(2:end)-P(1:end-1)) == p+1 );                                                                   % vyber jen historii s danym poctem koincidenci
     ind = find( (Data(P(w)+A,15) > Emin)  &  (Data(P(w)+A,15) < Emax) );                                          % vyber jen historii, kdy energii fluorescencniho fotonu ma A-tá udalost
     
     if p == 2                         % jen dvojne koincidence
       Data0                         = Data;                                                                         
       Data0(P(w(ind))+(p+1-A),[ 2]) = sign(Data(P(w(ind))+(p+1-A),[ 2])).*(100+abs(Data(P(w(ind))+(p+1-A),[ 2])));   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
       Data0(P(w(ind))+(p+1-A),[15]) =      Data(P(w(ind))+(p+1-A),[15]) +          Data(P(w(ind))+ A,     [15])  ;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
       Data0(P(w(ind))+ A,        :) = [];
       
     elseif p == 3                     % jen trojne koincidence
       radekData0 = 1:size(Data,1);                                 % index vsech radku puvodniho Data
       radekData3 = P(w(ind));                                      % index radku zacatku vypisu trojnych koincidenci v puvodnim Data, kdy A-ta udalost ma energii fluorescencniho fotonu
       Data0      = zeros(size(Data,1)+length(ind),size(Data,2));   % rozsirena matice Data
       
       for B = 1:length(ind)                
         radekData0(P(w(ind(B)))+4:end) = 2       + radekData0(P(w(ind(B)))+4:end);    % indexy radku pod kazdou vybranou trojnou udalosti se zvysi o 2 radky
         radekData3(B)                  = 2*(B-1) + radekData3(B);                     % index radku zacatku vypisu trojnych koincidenci v puvodnim Data se zvysi o 2 radky, kdy A-ta udalost ma energii fluorescencniho fotonu
       end
       
       in = 1:3;
       X  = [min(in(in-A ~= 0)) max(in(in-A ~= 0))];                                                         % poradova cisla udalosti v trojne koincidenci, ktera jsou doplnkova k vybrane A-te udalosti
       
       Data0(radekData0,:) = Data(:,:);
       %	nejprve se energie fluorescencniho fotonu priradi prvni udalosti
       Data0(radekData3 + 1,  : ) =      Data(P(w(ind))+X(1),  : );                                          % zkopirovani celeho radku prvni udalosti
       Data0(radekData3 + 1,[ 2]) = sign(Data(P(w(ind))+X(1),[ 2])).*(100+abs(Data(P(w(ind))+X(1),[ 2])));   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
       Data0(radekData3 + 1,[15]) =      Data(P(w(ind))+X(1),[15]) +          Data(P(w(ind))+A,   [15])  ;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
       Data0(radekData3 + 2,  : ) =      Data(P(w(ind))+X(2),  : );                                          % zkopirovani celeho radku druhe udalosti
       % 			Data0(radekData3 + 1,  : ) = 0;   % zkopirovani celeho radku prvni udalosti
       % 			Data0(radekData3 + 1,[ 2]) = 0;   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
       % 			Data0(radekData3 + 1,[15]) = 0;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
       % 			Data0(radekData3 + 2,  : ) = 0;   % zkopirovani celeho radku druhe udalosti
       %	pak se energie fluorescencniho fotonu priradi druhe udalosti
       Data0(radekData3 + 3,  : ) =      Data(P(w(ind))     ,  : );                                          % zkopirovani celeho prvniho radku historie
       Data0(radekData3 + 4,  : ) =      Data(P(w(ind))+X(1),  : );                                          % zkopirovani celeho radku prvni udalosti
       Data0(radekData3 + 5,  : ) =      Data(P(w(ind))+X(2),  : );                                          % zkopirovani celeho radku druhe udalosti
       Data0(radekData3 + 5,[ 2]) = sign(Data(P(w(ind))+X(2),[ 2])).*(100+abs(Data(P(w(ind))+X(2),[ 2])));   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
       Data0(radekData3 + 5,[15]) =      Data(P(w(ind))+X(2),[15]) +          Data(P(w(ind))+A,   [15])  ;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
       % 			Data0(radekData3 + 3,  : ) = 0;   % zkopirovani celeho prvniho radku historie
       % 			Data0(radekData3 + 4,  : ) = 0;   % zkopirovani celeho radku prvni udalosti
       % 			Data0(radekData3 + 5,  : ) = 0;   % zkopirovani celeho radku druhe udalosti
       % 			Data0(radekData3 + 5,[ 2]) = 0;   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
       % 			Data0(radekData3 + 5,[15]) = 0;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
     end
   end
   
   Data0(Data0(:,1) == 0,:) = [];
   L2 = size(Data0,1) - L1;                 % zmena velikosti matice. pocet oprav(dvojne koincidence -> jednoudalost) = abs(L2); pocet oprav(trojne koincidence -> dvojne koincidence) = L2/2;
   
   
   
   % 	if p == 2                         % dvojne koincidence
   % 		for A = 1:p
   % 			P   = find(  Data(:,1)>0);                                                                                    % cisla radku odpovidajici prvni udalosti v historii
   % 			w   = find( (P(2:end)-P(1:end-1)) == p+1 );                                                                   % vyber jen historii s danym poctem koincidenci
   % 			ind = find( (Data(P(w)+A,15) > Emin)  &  (Data(P(w)+A,15) < Emax) );                                          % vyber jen historii, kdy energii fluorescencniho fotonu ma A-tá udalost
   
   % 			Data(P(w(ind))+(p+1-A),[ 2]) = sign(Data(P(w(ind))+(p+1-A),[ 2])).*(100+abs(Data(P(w(ind))+(p+1-A),[ 2])));   %  2) Pořadové číslo interakce primárního fotonu; absolutni hodnota >100 znamena, ze k energii elektronu generovanem touto interakci byla prictena energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu
   % 			Data(P(w(ind))+(p+1-A),[15]) =      Data(P(w(ind))+(p+1-A),[15]) +          Data(P(w(ind))+ A,     [15])  ;   % 15) Celková energie deponovaná v této události tímto elektronem; sem se pricte energie deponovana elektronem, ktery vznikl interakci fluorescencniho fotonu	
   % 			Data(P(w(ind))+ A,        :) = [];
   % 		end
   % 	end
   
 endfunction
 
 
  %================================================================================

 
 function [Data,vzdMin0,cc,elinfo] = READ_PTRAC_OR_ELIST(soubor,S,siz,Etot)

%	parametry pro zpracování nyní zadávám v grafickém prostředí Matlabu. Ty se pak všechny uloží do struktury INP a používají v průběhu zpracování.
%	INP.Fv_energyCal: 0/1 - zda se má provést energetická rekalibrace. Nutný samostatný soubor s kalibrací.
%	INP.Fv_sourceFiles: 1/2/3 - typ vstupních souborů: 1 = MCelist, 2 = elist, 3 = obojí 
%	INP.Fv_findCoincidences: 0/1 - zda se mají v naměřeném elistu hledat koincidence (původně to měli v Advacamu blbě, tak jsem koincidence z naměřených elistů hledal sám)
%	INP.Fv_findCoincidencesWindow: čas v ns - šířka okna pro hledání událostí v koincidenci. Používám 200 ns.
%	INP.Fv_plotTime: 0/1 - zda se má zobrazit histogram rozdílů časů mezi následujícími událostmi - z toho je pak hezky vidět, počet koincidencí i nezbytná šířka časového okna
%	INP.Fv_plotTimeShow: 0/1 - zda graf zobrazí anebo jen uloží

%	seznam = MCNP_Seznam([soubor(1:end-5),'-Ecal.txt'],'soubor');
%	if INP.Fv_energyCal == 1  &&  INP.Fv_sourceFiles == 2  &&  isempty(seznam) == 1
%		error('   Energy re-calibration should be done but the calibration file %s was not found!',[soubor(1:end-5),'-Ecal.txt'])
%	end;

	display(sprintf('Reading the file  " %s "  (%i / %i)',soubor(1:end),S,siz))

  %====================
  %(A) ELIST
  %====================
	if strcmp(soubor(end-3:end),'elst') == 1                                    % jedna-li se o format elist, cc = 2 pro elist ze simulaci a cc = 3 pro elist z mereni

    soubor
    elinfo = READ_ELIST_HEADER(soubor);


			%	elinfo(1) .. pocet "header lines"
			%	elinfo(2) .. =2: Monte Carlo elist file, =3: measured elist file
			%	elinfo(3) .. pocet sloupcu v elist
			%	elinfo(4) .. posledni minimalni vzdalenost mezi eventy v um
			%	elinfo(5) .. pocet predchozich zpracovani elist souboru
			%	elinfo(11).. cislo sloupce promenne 1. DetectorID
			%	elinfo(12).. cislo sloupce promenne 2. EventID
			%	elinfo(13).. cislo sloupce promenne 3. x
			%	elinfo(14).. cislo sloupce promenne 4. y
			%	elinfo(15).. cislo sloupce promenne 5. E
			%	elinfo(16).. cislo sloupce promenne 6. t
			%	elinfo(17).. cislo sloupce promenne 7. Flags
			%	elinfo(18).. cislo sloupce promenne 8. z_sim
			%	elinfo(19).. cislo sloupce promenne 9. E0_sim
		cc = elinfo(2);

   
		fid  = fopen(soubor,'r');                                               % otevreni souboru elist
		eData = cell2mat(textscan(fid,[repmat('%f ',1,elinfo(3)-1),'%f'],'Delimiter',';','HeaderLines',elinfo(1)));           % nacte cely soubor s poctem sloupcu stanoveny pri prvnim nacteni
		fclose(fid);
		eData = [eData; [eData(end,1) eData(end,2)+1 eData(end,3:5) eData(end,6)+1E6 eData(end,7:end)]];                      % pridani umele vytvoreneho radku na konec souboru, aby nebyl ztracena jedna posledni historie

%	----------------------------------
%   apply to measured data only
%	----------------------------------

		if cc == 3 

    %	==== oprava casu udalosti na kontinualni tok casu ====
			u = find(eData(2:end,2)-eData(1:end-1,2) == 0);                     % bude uvazovat jen casy prvni udalosti v koincidenci (pro pripad, ze by udalosti v koincidenci byly casove prehazene)
			v = setdiff(1:size(eData,1),u+1);                                   % cisla radku prvnich udalosti
			T = eData(v(2:end),6) - eData(v(1:end-1),6);                        % rozdil casu prvnich udalosti
			
			T(T>0) = 0;                                                         % nechame jen zaporne casy
			T(T<0) = -T(T<0) + 100*1;%INP.Fv_findCoincidencesWindow;               % prevod na kladne hodnoty a pridani 100*sirku okna, aby nebyly uznany jako koincidence
%			sumaT = sum(T)
			
			eData(v(2:end),6) = eData(v(2:end),6) + cumsum(T);                  % prevod casu prvnich udalosti na absolutni casy; casy dalsich udalosti v koincidenci jsou stale relativni
			 

%	==== hledani koncidenci ====
			%if  INP.Fv_findCoincidences == 1    
			%	display(sprintf('  Re-search for coincidences. Time window set to %1.4g ns.',INP.Fv_findCoincidencesWindow))

				u = [1; intersect(find(eData(2:end,2)-eData(1:end-1,2) == 0),1+find(eData(2:end,2)-eData(1:end-1,2) == 1))];  % cislo radku prvni udalosti ve vicenasobne koincidenci
				for A = 1:length(u)    % toto je rychly skript
					B = 1;
					while eData(u(A)+B,2) == eData(u(A),2)
						eData(u(A)+B,6) = eData(u(A)+B,6) + eData(u(A),6);      % prepocet relativnich casu koincidencnich udalosti na absolutni
						B = B + 1;
					endwhile;
				endfor;
%				figure(1); plot(eData(:,6));

				eData00 = eData;
				co = find(abs(eData(2:end,6)-eData(1:end-1,6)) < 1);%INP.Fv_findCoincidencesWindow);              % najde po sobe nasledujici udalosti S koincidenci
				
				eData(1:co(1),2) = 0:co(1)-1;
				for A = 2:length(co)
					eData(co(A-1)+1:co(A),2) = eData(co(A-1),2) + (0:(co(A)-co(A-1)-1))';   % precislovani poradovych cisel koincidenci
				endfor;
				eData(co(end)+1:end,2) = eData(co(end),2) + (0:(size(eData,1)-co(end)-1))';
				
				u = [1; intersect(find(eData(2:end,2)-eData(1:end-1,2) == 0),1+find(eData(2:end,2)-eData(1:end-1,2) == 1))];  % cislo radku prvni udalosti ve vicenasobne koincidenci
				for A = 1:length(u)    % toto je rychly skript
					B = 1;
					while eData(u(A)+B,2) == eData(u(A),2)
						eData(u(A)+B,6) = eData(u(A)+B,6) - eData(u(A),6);   % prepocet absolutnich casu koincidencnich udalosti na relativni
						B = B + 1;
					endwhile;
				endfor;

				display(sprintf('  Total number of histories (single events or coincidences with any number of events): before | after time window correction:  %i | %i.',eData00(end,2),eData(end,2)))
				display(sprintf('  Number of coincidences before | after time window correction:  %i | %i (x%1.2f).', length(find(eData00(3:end,2)-2*eData00(2:end-1,2)+eData00(1:end-2,2)==-1)), length(find(eData(3:end,2)-2*eData(2:end-1,2)+eData(1:end-2,2)==-1)),   length(find(eData(3:end,2)-2*eData(2:end-1,2)+eData(1:end-2,2)==-1))/length(find(eData00(3:end,2)-2*eData00(2:end-1,2)+eData00(1:end-2,2)==-1))  ));
			%endif;
			
%	==== rekalibrace deponovanych energii ====
	%		if INP.Fv_energyCal == 1  &&  INP.Fv_sourceFiles == 2
	%			fid  = fopen([soubor(1:end-5),'-Ecal.txt'],'r');                                                  % otevreni souboru s energetickou kalibraci
	%			Ecal = cell2mat(textscan(fid,'%f %f','Delimiter',';','HeaderLines',1,'MultipleDelimsAsOne',1));   % nacte cely soubor s poctem sloupcu stanoveny pri prvnim nacteni
	%			fclose(fid);
	%			eData(:,elinfo(15)) = interp1(Ecal(:,2),Ecal(:,1),eData(:,elinfo(15)),'linear');                  % rekalibrace energie
%	priklad:
% E nova (keV); E puvodni (keV);
%     0.14;    0.1;
%    14   ;   10  ;
%    24.9 ;   18  ;
%    30.8 ;   23  ;
%    32   ;   23.5;
%    39.8 ;   33.5;
%    59.5 ;   54.8;
%    81.0 ;   76.9;
%   121.8 ;  118.9;
%   244.7 ;  239.8;
%   344.3 ;  337.9;
%   356.0 ;  348.1;
%   661.7 ;  644.5;
%  1332.5 ; 1299.0;
% 10257.9 ;10000.0;
			%endif;
%	----------------------------------
		endif; %only measured data 
%	----------------------------------

%	==== prevod elist eData na PTRAC Data =====

		Data0 = zeros(size(eData,1),34);                                        % prevod elist eData na PTRAC Data
		if elinfo(18) ~= 0  &&  elinfo(19) ~= 0 
			Data0(:,[6 1 7 8 15 28 9 16]) = eData(:,elinfo([11:16 18:19]));
		elseif elinfo(18) ~= 0  &&  elinfo(19) == 0 
			Data0(:,[6 1 7 8 15 28 9   ]) = eData(:,elinfo([11:16 18   ]));     % E0 = 0 defaultne
		elseif elinfo(18) == 0  &&  elinfo(19) ~= 0 
			Data0(:,[6 1 7 8 15 28   16]) = eData(:,elinfo([11:16    19]));     % z = 0 defaultne
		else
			Data0(:,[6 1 7 8 15 28     ]) = eData(:,elinfo([11:16      ]));     % z = 0 a E0 = 0 defaultne
		end;
		Data0(:, 7)      = 1/10*(Data0(:, 7)) - 0.7041;                         % prevede mm na cm a posune na stred cipu
		Data0(:, 8)      = 1/10*(Data0(:, 8)) - 0.7041;                         % prevede mm na cm a posune na stred cipu
		Data0(:, 9)      = 1/10*(Data0(:, 9)) - 0;                              % prevede mm na cm a posune
		Data0(:,[15 16]) = 1E-3* Data0(:,[15 16]);                              % prevede keV na MeV
		
		P0b      = [1; 1+find(Data0(2:end,1)-Data0(1:end-1,1) == 1)];           % cisla radku prvni udalosti v historii
		P0b(:,2) = (1:length(P0b))';                                            % poradove cislo historie
		Data     = zeros(size(P0b,1)+size(eData,1),34);                         % pridan jeden radek ke kazde unikatni historii, aby format odpovidal vystupu z PTRAC
%	===== historie s poctem udalosti vyssi jak zadany pocet nebudou brany v uvahu ======
		MX = min([ 5  max(P0b(2:end,1)-P0b(1:end-1,1))]);                       % nejvyssi pocet udalosti v jedne historii, ale maximalne X udalosti v koincidenci
%		MX =          max(P0b(2:end,1)-P0b(1:end-1,1));                         % nejvyssi pocet udalosti v jedne historii, ale maximalne X udalosti v koincidenci
%	====================================================================================
%		tic
    
		for p = 1:MX                                                            % pres vsechny pocty udalosti v jedne historii (1,2,az do max.poctu udalosti v jedne historii)
			w = find( (P0b(2:end,1)-P0b(1:end-1,1)) == p );                     % najde historie s danym poctem udalosti
			
			if isempty(w) == 0                                                  % pokud existuje alespon jedna historie s danym poctem udalosti
				for q = 1:p                                                     % pres vsechny jednotlive udalosti v kazde historii s danym poctem udalosti
					Data((P0b(w,2))+P0b(w,1)+(q-1), :) =       Data0(           P0b(w,1)+(q-1), :);   % zkopiruje vsechny sloupce postupne ve vsech radcich dane historie
					Data((P0b(w,2))+P0b(w,1)+(q-1),28) =  1/10*Data( (P0b(w,2))+P0b(w,1)+(q-1),28);   % cas udalosti krome prvni
					Data((P0b(w,2))+P0b(w,1)+(q-1), 1) = -1;
					Data((P0b(w,2))+P0b(w,1)+(q-1), 1) = -1;
					Data((P0b(w,2))+P0b(w,1)-1,    15) =  Data((P0b(w,2))+P0b(w,1)-1,15) + Data((P0b(w,2))+P0b(w,1)+(q-1),15);    % celkova deponovana energie v historii
				end;
				
				Data((P0b(w,2))+P0b(w,1)-1,28) =  1/10*Data((P0b(w,2))+P0b(w,1),28);                            % cas vzniku primarniho fotonu; v pripade mereni = cas prvni udalosti
				Data((P0b(w,2))+P0b(w,1)-1, 1) =  1  + Data0(P0b(w,1), 1);      % ID
				
				if elinfo(19) == 0                                              % energy of primary photon
					Data((P0b(w,2))+P0b(w,1)-1,16) = Etot;                     % E0 defaultne
				else
					Data((P0b(w,2))+P0b(w,1)-1,16) = Data((P0b(w,2))+P0b(w,1),16);   % E0 zname                                    
					for q = 1:p
						Data((P0b(w,2))+P0b(w,1)+(q-1),16) = 0;
					endfor;
				endif; %mame/nemame informaci o energii primarniho fotonu
			endif; %existuje alespon jedna historie s danym poctem udalosti
		endfor; %pres vsechny pocty udalosti v jedne historii
%		toc
		Data(sum(Data,2)==0,:) = [];                                            % smaze radky historii s vice koincidencemi, nez zadana mez
		Data(:,13) = Data(:,16);                                             % E0 zkopirujeme i do 13.sloupce
		
		if cc == 3                                                              % elist z mereni
			vzdMin0 = 110E-4;
		else                                                                    % elist ze simulaci
			vzdMin0 = elinfo(4);
		endif;

		display(sprintf('  Reading of elist file finished.'));

  %====================
  %(B) PTRAC
  %====================
	else                                         % jedna-li se o vystup z PTRAC
		cc = 1;
		rozbaleno = unzip(soubor);               % rozbali soubor s vypisem historii        
		fid  = fopen(rozbaleno{1},'r');          % rozbalení a otevreni souboru s vypisem historii
		
		vzdMin0 = cell2mat(textscan(fid,'%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %f %*s','Delimiter',' ','HeaderLines',3,'MultipleDelimsAsOne',1));    % minimalni vzdalenost interakci pouzita v UTEF_ptrac_2019.m"
		Data = cell2mat(textscan(fid,repmat(['%f '],1,34),'Delimiter',',','HeaderLines',7));                                                                   % nacte soubor s vypisem historii, ktere byly vygenerovany skriptem "UTEF_ptrac_2019.m"
		fclose(fid);
		delete(rozbaleno{1})
		
		elinfo = [0 cc 9 -1 0  0 0 0 0 0  1 2 3 4 5 6 7 8 9];
		display(sprintf('  MCelist file read.'));
   
	endif;

endfunction	
	

 %================================================================================

	
function elinfo = READ_ELIST_HEADER(soubor)
%	elinfo(1) .. pocet "header lines"
%	elinfo(2) .. =2: Monte Carlo elist file, =3: measured elist file
%	elinfo(3) .. pocet sloupcu v elist
%	elinfo(4) .. posledni minimalni vzdalenost mezi eventy v um
%	elinfo(5) .. pocet predchozich zpracovani elist souboru
%	elinfo(11).. cislo sloupce promenne 1. DetectorID
%	elinfo(12).. cislo sloupce promenne 2. EventID
%	elinfo(13).. cislo sloupce promenne 3. x
%	elinfo(14).. cislo sloupce promenne 4. y
%	elinfo(15).. cislo sloupce promenne 5. E
%	elinfo(16).. cislo sloupce promenne 6. t
%	elinfo(17).. cislo sloupce promenne 7. Flags
%	elinfo(18).. cislo sloupce promenne 8. z_sim
%	elinfo(19).. cislo sloupce promenne 9. E0_sim

	elinfo = zeros(1,20);
    
%	==== najde pocet "header lines" ====
	fid  = fopen(soubor,'r');                                                   % otevreni souboru elist
	m = 1; elinfo(1) = 0;      

  m=1;
	while m < 2                                                             
		r = textscan(fid,'%[#] %s',1,'Delimiter','\n');                        % nacte radek 
		if strcmp(r{1},'#')==0                                                %pokud radek nezacina #, muzeme uz cyklus ukoncit
      break;  
    endif;                            
		elinfo(1) = elinfo(1) + 1;                                              % pocet "header"
	endwhile;     

	fclose(fid);
	elinfo(1) = 2 + elinfo(1);  %there are two more header lines, which do not start with #, but contain description (variables and their units), not the data
		# 

%	==== zjisti, zda se jedna o Monte Carlo data ====
	fid  = fopen(soubor,'r');                                                   % otevreni souboru elist
	r    = textscan(fid,'%*s %s',1,'Delimiter',' ');                            % nacte druhy string v prvnim radku, zda se jedna o slovo "Monte"
	fclose(fid);
	if strcmp(r{1},'Monte') == 1  ||  strcmp(r{1},'MCdata:;') == 1
		elinfo(2) = 2;        % elist z elistu ze simulace
	else
		elinfo(2) = 3;        % elist z mereni
	end;
	
%	==== zjisti posledni minimalni vzdalenost mezi eventy v um ====
	elinfo(4) = -1;
	if elinfo(2) == 2
		fid  = fopen(soubor,'r');                                                                                     % otevreni souboru elist
		r    = cell2mat(textscan(fid,[repmat('%*s ',1,19), '%f %*s'],1,'Delimiter',' ','HeaderLines',elinfo(1)-2));   % nacte druhy string v prvnim radku, zda se jedna o slovo "Monte"
		fclose(fid);
		
		if isnan(r) == 0
			elinfo(4) = r;
		end;
	end;
	
%	==== zjisti pocet predchozich zpracovani elist souboru ====
	fid  = fopen(soubor,'r');                                                   % otevreni souboru elist
	for A = 1:elinfo(1)-2
		r = textscan(fid,'%*s %s %*[^\n]',1,'Delimiter',' ');                            
		if strcmp(r{1},'Re-processed') == 1  |  strcmp(r{1},'Processed') == 1
			elinfo(5) = elinfo(5) + 1;                                          % pocet "processed"
		end;  
	end;                                                                   
	fclose(fid);
%	==== zjisti pocet sloupcu dat ====
	fid  = fopen(soubor,'r');                                                   % otevreni souboru elist
	r1   = textscan(fid,'%s',1,'Delimiter','\n','HeaderLines',elinfo(1)-2);      % nacte radek s popisem sloupcu, aby zjistil, kolik ma
	fclose(fid);                                                                % zavre soubor elist
	
	elinfo(3) = 1+length(find(r1{1}{1}==';'));                                  % pocet sloupcu
	
%	==== zjisti cislo sloupcu ====
	fid  = fopen(soubor,'r');                                                   % otevreni souboru elist
	r1   = textscan(fid,[repmat('%s ',1,length(find(r1{1}{1}==';'))),'%s'],1,'Delimiter',';','HeaderLines',elinfo(1)-2);  % nacte radek s popisem sloupcu, aby zjistil, kolik ma
	fclose(fid);                                                                % zavre soubor elist
	sl = find(strcmp([r1{:}], 'DetectorID'));  if isempty(sl) == 0;  elinfo(11) = sl;  end;    % cislo sloupce promenne DetectorID
	sl = find(strcmp([r1{:}], 'EventID'));     if isempty(sl) == 0;  elinfo(12) = sl;  end;    % cislo sloupce promenne EventID   
	sl = find(strcmp([r1{:}], 'x'));           if isempty(sl) == 0;  elinfo(13) = sl;  end;    % cislo sloupce promenne x         
	sl = find(strcmp([r1{:}], 'y'));           if isempty(sl) == 0;  elinfo(14) = sl;  end;    % cislo sloupce promenne y         
	sl = find(strcmp([r1{:}], 'E'));           if isempty(sl) == 0;  elinfo(15) = sl;  end;    % cislo sloupce promenne E         
	sl = find(strcmp([r1{:}], 't'));           if isempty(sl) == 0;  elinfo(16) = sl;  end;    % cislo sloupce promenne t         
	sl = find(strcmp([r1{:}], 'Flags'));       if isempty(sl) == 0;  elinfo(17) = sl;  end;    % cislo sloupce promenne Flags     
	sl = find(strcmp([r1{:}], 'z'));           if isempty(sl) == 0;  elinfo(18) = sl;  end;    % cislo sloupce promenne z_sim     
	sl = find(strcmp([r1{:}], 'z_sim'));       if isempty(sl) == 0;  elinfo(18) = sl;  end;    % cislo sloupce promenne z_sim     
	sl = find(strcmp([r1{:}], 'E0_sim'));      if isempty(sl) == 0;  elinfo(19) = sl;  end;    % cislo sloupce promenne E0_sim    
 endfunction
 
 
  %================================================================================
 
 
 function SAVE_ELIST(Data,P0b,soubor,dat1,dat2,th,cc,vz)
   
   if cc(1,4) == 1          % jen vystup z PTRAC
     %		display(sprintf('Saving the elist file...'));
     eData = Data;
     
     for A = 1:length(P0b)-1
       eData(P0b(A)+1:P0b(A+1)-1, 1) = A-1;                                                        % EventID
       
       if P0b(A+1)-P0b(A)>2                                                                        % viceudalostni historie seradi podle casu intrakce
         eData(P0b(A)+1:P0b(A+1)-1,:) = sortrows(eData(P0b(A)+1:P0b(A+1)-1,:),28);
       end
       
       eData(P0b(A)+1:P0b(A+1)-1,28) = 10*(eData(P0b(A)+1:P0b(A+1)-1,28) - eData(P0b(A)+1,28));    % t (ns); prevod ze "shakes" na ns
     end
     
     eData(P0b(end)+1:end,:) = [];                 % smaze posledni historii
     eData(P0b,:)= [];                             % smaze udalost oznacujici misto vzniku primarniho fotonu
     eData(:, 7) = 10* (eData(:, 7) + 0.7041);     % prevede cm na mm a posune do rohu
     eData(:, 8) = 10* (eData(:, 8) + 0.7041);     % prevede cm na mm a posune do rohu
     eData(:, 9) = 10* (eData(:, 9) + 0     );     % prevede cm na mm a posune
     eData(:,15) = 1E3* eData(:,15);               % prevede MeV na keV
     eData(:,[2:6 10:14 16:27 29:34]) = [];        % smaze nepotrebne sloupce
     
     eData = [eData -2*ones(size(eData,1),9)];
     %		eData(1:100,:)
     
     fid = fopen(['MC-',char(soubor),'_',dat1,'.elst'],'w');
     fprintf(fid,'%s\n',['# MCdata:; file ',char(soubor),';date: ',dat2,', correction for fluorescence photons (yes=1): ',int2str(cc(1)),';energy threshold: ',num2str(th,'%1.2f'),' keV;minimum distance between events: ',num2str(vz,'%1.0f'),' um']);
     % 		fprintf(fid,'%s\n','EventID;x;y;z;E;t;Size;Height;Roundness;Linearity;Length;Width;PolarAngle;BorderPixCount;InnerPixCount')
     % 		fprintf(fid,'%s\n','-;mm;mm;mm;keV;ns;pix;keV;-;-;mm;mm;rad;1;1')
     % 		fprintf(fid,['%i;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g'],[])
     fprintf(fid,'%s\n','EventID;x;y;z;E;t;Size;Height;Roundness;Linearity;Length;Width;PolarAngle;BorderPixCount;InnerPixCount');
     fprintf(fid,'%s\n','-;mm;mm;mm;keV;ns;pix;keV;-;-;mm;mm;rad;-;-');
     fprintf(fid,['%i;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g\n'],eData(1:end-1,:)');
     fprintf(fid,['%i;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g;%1.5g'],  eData(  end,  :)');
     fclose(fid);    
     
     % 		EventID
     % 		x;y;z;  Data(..,[7:9])
     % 		E; Data(...,[15])
     % 		t; Data(...,[28]) *1e-8 s
     % 		Size;Height;Roundness;Linearity;Length;Width;
     % 		PolarAngle;
     % 		BorderPixCount;InnerPixCount
   end
 endfunction
 

 %================================================================================

 
 function sez = MCNP_seznam(maska,typ,varargin)
   %	ulozi  seznam souboru s danou maskou
   %	maska: maska souboru, napr.: '*.jpg'
   %   typ:   soubor - najde pouze soubory
   %          adresar - najde pouze adresare
   %          vsechno - najde soubory i adresare
   %	varargin: 'error' - pokud v adresari nebudou zadne takove soubory, pak hodi chybu
   
   [ano_t,index] = MCNP_varargin(varargin,'soubor');   % hodi chybu, pokud nenajde zadne soubory
   
   if strcmp(maska(end), '.' ) == 1         % pokud chceme soubory bez pripony, je nutny specialni postup
     sez1 = struct2cell(dir([maska(1:end-1),'.*']));      % vsechny soubory s priponou
     sez2 = struct2cell(dir([maska(1:end-1),'*' ]));      % vsechny soubory i adresare
     [sez3,c] = setdiff(sez2(1,:),sez1(1,:));   % vsechny soubory bez pripony a adresare
     sez = sez3(cell2mat(sez2(4,c)) == 0)';     % vsechny soubory bez pripony
   else
     
     % === pro vyber jen souboru s priponou ===
     sez = struct2cell(dir(maska));             % nacte se seznam souboru a adresaru
     sez(:,strcmp(sez(1,:), '.' )) = [];        % odstrani '.'
     sez(:,strcmp(sez(1,:), '..')) = [];        % odstrani '..'
     
     if exist('typ','var')   % pokud chceme jen adresare nebo jen soubory
       if strcmp(typ, 'adresar') == 1            % chceme jen adresare
         sez(:,cell2mat(sez(4,:)) == 0) = [];  % odstrani soubory
       else                                      % chceme jen soubory
         sez(:,cell2mat(sez(4,:)) == 1) = [];  % odstrani adresare
       end
     end
     
     sez = sez(1,:)';   % nazvy souboru jsou jen v prvnim radku
     %	===================== ANEBO: ============
     %	d = dir(maska)                                          % Select only files with specific extension
     %	d = d(cellfun(@length,{d(:).name})>2);                                     % jen nazvy delsi nez 2 znaky; puvodne: d = d(find(cellfun(@length,{d(:).name})>2));
     %	
     %	sez = d(:).name
     %	============================
     
   end
   
   if isempty(sez) == 1   % pokud nenajde zadne soubory
     [ano,index] = MCNP_varargin(varargin,'error');   % hodi chybu, pokud nenajde zadne soubory
     
     if ano == 1  % pokud ma hodit chybu
       display(sprintf(' '))
       error(sprintf('===== ERROR: No files/directory found!  (Mask = %s)! =====',maska));
     end
   end
   
 endfunction
 

 %================================================================================

 
 function [nasel,misto] = MCNP_varargin(vstup,parametr)
   % nasel - 0=neni ve vstupu pozadovany parametr, 1=je tam pozadovany parametr
   % misto - parametr je na (misto)-te pozici ve vektoru vstup
   % vstup - kde se hleda parametr
   % parametr - nazev hledaneho parametru
   
   nasel = 0;
   misto = -1;
   
   [a,b] = find(strcmp(vstup,parametr) == 1);
   
   if b > 0
     nasel = 1;
     misto = b;
   end
 endfunction
 
 
 
 %=========================================================================
 %               ORIGIN ENSEMBLES FUNCTIONS
 %=========================================================================
 
 
 
 function [xyz_ROI,solution_found] = point_on_cone(vaxis,theta_compton,cone_height,origin,ROI,nrand=100,max_trials=10,weight=1.0)
  %input parameters:
  %axis - cone axis (vector)
  %theta_compton - half of the apex angle of the cone
  %cone_height - height of the cone
  %origin - move the coordinate system to this point
  %ROI - region of interest (rectangular parallelepiped, 3x2 matrix)
  %nrand - number of random nubers to generate - (low number speeds up the calculation, howerver it increases the chance that there will be no solution satisfying the ROI conditions)
  %max_trials - maximal number of trials to find a solution (in order to avoid infinite while loop)
  %weight: weight<1.0 -> points will be generated predominantly further away from the cone apex; weight>1.0 -> -||- closer to the cone apex
  
  solution_found=0;
  trial=0; #counter
  xyz_ROI=zeros(1,3); #prepare output matrix
  
  while solution_found==0 %if the acceptable solution is not found, we will generate a new set of random numbers and repeat the whole process
    
    trial=trial+1;
    if trial > max_trials %we have reached the maximum number of trials
      break;
    endif
    
    %radial coordinates of the cone axis (in the lab-frame)
    %theta=pi/4;
    %phi=pi;
    %r=1;#z_axis_cone should be unit vector, therfore r=1
    %rxy=r*sin(theta);
    
    %generate random points on the surface of the cone in the cone-coordinate-frame
    rand1=rand(nrand,1).^weight; %weight<1.0 -> points will be generated predominantly further away from the cone apex; weight>1.0 -> -||- closer to the cone apex
    rand2=rand(nrand,1);
    theta_cone=theta_compton; #half of the apex angle
    height=cone_height; %height of the cone
    z_cone=height*sqrt(rand1); %random position in the z-axis - we want uniform distribution on the SURFACE of the cone, not uniform distribution in the height -> z_cone ~ sqrt(random_number)
    r_cone=z_cone*tan(theta_cone);
    phi_cone=2*pi*rand2;
    x_cone=r_cone.*cos(phi_cone);
    y_cone=r_cone.*sin(phi_cone);
    
    %calculate positions of the cone-coordinate-frame's axes in the lab-frame
    %z_axis_cone=[rxy*cos(phi); rxy*sin(phi); r*cos(theta)]; #z-axis is defined by the line connecting the two events
    z_axis_cone=vaxis'; #z-axis is defined by the line connecting the two events
    if z_axis_cone(1)==0 #z_axis_cone lies in the z-y plane of the lab frame => x_axis_cone can be identical with x axis of the lab-frame
      x1=1; x2=0;
    else 
      x2=sqrt(1/(1+(z_axis_cone(2)/z_axis_cone(1))^2));
      x1=-x2*z_axis_cone(2)/z_axis_cone(1);
    endif
    x_axis_cone=[x1; x2; 0];%x-axis of the cone frame is chosen so it lies in the x-y plane of the lab frame, is perpendicular to z axis of the cone frame, it is a unit vector
    y_axis_cone=cross(z_axis_cone,x_axis_cone);%y-axis is perpendicular to z and x axis of the cone frame, it is a unit vector
    
    %create transformation matrix from cone frame to lab-frame
    T=[x_axis_cone y_axis_cone z_axis_cone];
    %calculate position of the generated points in the lab-frame
    XYZ_lab=T*[x_cone'; y_cone'; z_cone'];
    %shift the local lab-frame (centered at the position of the fired pixel) to the global lab-frame (centered at the sensor's center)
    origins=repmat(origin,nrand,1);
    XYZ_lab=XYZ_lab'+origins;
    
    %figure(fig_no);
    %scatter3 (XYZ_lab(:,1), XYZ_lab(:,2), XYZ_lab(:,3), 10, z_cone(:), "o");
    %hold on
    %scatter3 (-4, 0, 0, 30, "r", "+");
    %hold on

    
    %check the ROI condition
    in_ROI=find((XYZ_lab(:,1)>ROI(1,1)) & (XYZ_lab(:,1)<ROI(1,2)) & (XYZ_lab(:,2)>ROI(2,1)) & (XYZ_lab(:,2)<ROI(2,2)) & (XYZ_lab(:,3)>ROI(3,1)) & (XYZ_lab(:,3)<ROI(3,2)));
    XYZ_ROI=XYZ_lab(in_ROI,:);
    if size(XYZ_ROI,1) > 0;
      solution_found=1;
      xyz_ROI=XYZ_ROI(1,1:3);
      %scatter3 (xyz_ROI(1), xyz_ROI(2), xyz_ROI(3), 50, "b", "x");
      %hold off
    endif
   
  endwhile
  
endfunction


 %================================================================================


 function nevents = event_density(center, max_distance, points)
   %calculates total number of points from nx3 matrix "points" within distance "max_distance" from the point "center"
   centers=repmat(center,size(points,1),1);
   distance = norm( centers - points , 2, "rows" ); %the Euclidean distance between two vectors is the two-norm of their difference
   max_distances=repmat(max_distance,size(points,1),1);
   near=find(distance < max_distances);
   nevents=size(near,1); % the point "center" is always near (its distance from itself is 0), therefore nevents>=1
 endfunction

 
