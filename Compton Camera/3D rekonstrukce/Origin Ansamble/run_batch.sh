#!/bin/bash

FIGPATH="fig/kriz_sim_Esmeared_loop-Eres_Ecorr"
DESC="even density of generated points, wider ROI in z, elist: simulace kriz, E=365+-12, Esmear; loop: Eres, Ecorr"

mkdir $FIGPATH
cp run_batch.sh $FIGPATH/

for NEVENTS in 1000 #1000
do
for MCSWEEPS in 2000 #1000 2000 3000 4000 
do
for DENS_RADIUS in  2.0 #0.1 0.5 1.0 
do
for ENERGY_SMEAR in 1 #0 1
do
for ERES in 0.001 0.02 0.05 0.1
do
for ENERGY_CORR in 1 2 3
do
for POS_CORR in 0 #0 1
do
for POS_SMEAR in 0 #0 1 2
do
for XSEC in 0
do
for PORADI in 1 #2 #2
do
for HWEIGHT in 1.0 #0.85 #0.85 0.9 0.95 1.0
do
if [ $ENERGY_CORR -lt 3 ] &&  [ $XSEC -eq 1 ]; then
    continue
fi

FULLPATH=$FIGPATH/nevents${NEVENTS}_nrand${NRAND}_energyCorr${ENERGY_CORR}_posSmear${POS_SMEAR}
mkdir -p $FULLPATH

octave --eval "ComptonC_OA_rekonstrukce($NEVENTS,$MCSWEEPS,$DENS_RADIUS,$ERES,'$FULLPATH','correct',$ENERGY_CORR,$POS_CORR,'smear', $ENERGY_SMEAR , $POS_SMEAR, 'xsec',$XSEC,'poradi',$PORADI, 'hweight',$HWEIGHT)"

done
done
done
done
done
done
done
done
done
done
done

cp run_batch.sh $FIGPATH/
cd $FIGPATH
echo $DESC > desc.txt
