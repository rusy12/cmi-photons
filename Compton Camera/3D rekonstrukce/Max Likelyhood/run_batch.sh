#!/bin/bash

FIGPATH="fig/kriz_motol_sim_EUnc_loop-Ecorr"
DESC="even density of generated points, elist: simulace kriz, E=365+-12, EUnc; loop: Ecorr"

mkdir -p $FIGPATH
cp run_batch.sh $FIGPATH/

for NEVENTS in 10000 #1000
do
for NRAND in 100 #300 500 1000 #1000 2000 3000 4000 
do
for DENS_RADIUS in 2.0 #0.1 10 #0.1 0.5 1.0 
do
for ENERGY_CORR in 1 2 # 1 0 2 3
do
for POS_CORR in 0 #0 1
do
for ENERGY_SMEAR in 0 #0 1
do
for POS_SMEAR in 0 #0 1 2
do
for XSEC in 0
do
for PORADI in 1 #2 #2
do
for HWEIGHT in 1.0 #0.85 #0.85 0.9 0.95 1.0
do
if [ $ENERGY_CORR -lt 3 ] &&  [ $XSEC -eq 1 ]; then
    continue
fi

FULLPATH=$FIGPATH/nevents${NEVENTS}_nrand${NRAND}_energyCorr${ENERGY_CORR}_posSmear${POS_SMEAR}
mkdir -p $FULLPATH


octave --eval "ComptonC_ML_rekonstrukce($NEVENTS,$NRAND,$DENS_RADIUS,'$FULLPATH','correct',$ENERGY_CORR,$POS_CORR,'smear', $ENERGY_SMEAR , $POS_SMEAR,'xsec',$XSEC,'poradi',$PORADI, 'hweight',$HWEIGHT)"

cat utils/root_plot.C >> $FULLPATH/results.C


done
done
done
done
done
done
done
done
done
done

cp run_batch.sh $FIGPATH/
cd $FIGPATH
echo $DESC > desc.txt
